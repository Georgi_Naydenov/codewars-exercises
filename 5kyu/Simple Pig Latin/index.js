function pigIt(str) {
    const regex = /^[A-Za-z]+$/;
    return str.split(' ').map(word => word.match(regex) ? word.slice(1) + word[0] + `ay` : word).join(' ');
}

console.log(pigIt('Pig latin is cool'));