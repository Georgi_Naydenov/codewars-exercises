function moveZeros(arr) {
    const nonZeroes = arr.filter(element => element !== 0);
    const zeroCount = arr.length - nonZeroes.length;
    return nonZeroes.concat(Array(zeroCount).fill(0));
}



//console.log(moveZeros([false, 1, 0, 1, 2, 0, 1, 3, "a"]));