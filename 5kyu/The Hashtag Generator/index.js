function generateHashtag(str) {
    if (str.trim() === "") return false;

    const upperCaseWords = str.split(" ").map(word => word.slice(0, 1).toUpperCase() + word.slice(1).toLowerCase()).join("");
    return `#${upperCaseWords}`.length > 140 ? false : `#${upperCaseWords}`;

}

console.log(generateHashtag("ywhc hznm aexy  "));