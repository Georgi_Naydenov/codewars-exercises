function alphanumeric(string) {
    const regex = /^[A-Za-z0-9]+$/;
    return string.match(regex) ? true : false;
}

console.log(alphanumeric('hello world_'));