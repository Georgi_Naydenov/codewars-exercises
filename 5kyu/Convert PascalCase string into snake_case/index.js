function toUnderscore(string) {
    if (typeof string !== 'string') {
      return string.toString();
    }
  
    const result = string.replace(/[A-Z]/g, (match) => `_${match.toLowerCase()}`);
    return result[0] === '_' ? result.slice(1) : result;
  }

//console.log(toUnderscore('App7Test'));