function zero(func) {
    if (typeof func === 'function') {
        return func(0);
    }
    return 0;
}

function one(func) {
    if (typeof func === 'function') {
        return func(1);
    }
    return 1;
}

function two(func) {
    if (typeof func === 'function') {
        return func(2);
    }
    return 2;
}

function three(func) {
    if (typeof func === 'function') {
        return func(3);
    }
    return 3;
}

function four(func) {
    if (typeof func === 'function') {
        return func(4);
    }
    return 4;
}

function five(func) {
    if (typeof func === 'function') {
        return func(5);
    }
    return 5;
}

function six(func) {
    if (typeof func === 'function') {
        return func(6);
    }
    return 6;
}

function seven(func) {
    if (typeof func === 'function') {
        return func(7);
    }
    return 7;
}

function eight(func) {
    if (typeof func === 'function') {
        return func(8);
    }
    return 8;
}

function nine(func) {
    if (typeof func === 'function') {
        return func(9);
    }
    return 9;
}

function plus(rightOperand) {
    return function (leftOperand) {
        return leftOperand + rightOperand;
    };
}

function minus(rightOperand) {
    return function (leftOperand) {
        return leftOperand - rightOperand;
    };
}

function times(rightOperand) {
    return function (leftOperand) {
        return leftOperand * rightOperand;
    };
}

function dividedBy(rightOperand) {
    return function (leftOperand) {
        return Math.floor(leftOperand / rightOperand);
    };
}

console.log(one(plus(two())));