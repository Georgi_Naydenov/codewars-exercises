function domainName(url) {
    const urlRegex = /^(?:https?:\/\/)?(?:www\.)?([^\/.]+)/;
    const match = url.match(urlRegex);
    if (match) {
        return match[1];
    }
}

console.log(domainName("https://www.cnet.com")); 
