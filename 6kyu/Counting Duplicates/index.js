function duplicateCount(text) {
    const map = new Map();
    let duplicates = 0;

    for (const letter of text) {
        const lowercaseLetter = letter.toLowerCase();

        if (!map.has(lowercaseLetter)) {
            map.set(lowercaseLetter, 1);
        } else if (map.get(lowercaseLetter) === 1) {
            map.set(lowercaseLetter, 2);
            duplicates++;
        }
    }

    return duplicates;
}

//console.log(duplicateCount('abcdefghijklmnopqrstuvwxyzbaaAAB'));