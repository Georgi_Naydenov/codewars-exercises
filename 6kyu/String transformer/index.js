function stringTransformer(str) {
    const string = str.split(" ").reverse();

    return string.map(item => item.split("").map(letter => isCapitalized(letter) ? letter.toLowerCase() : letter.toUpperCase()).join("")).join(" ")

}

function isCapitalized(letter) {
    return letter === letter.toUpperCase() && letter !== letter.toLowerCase();
}
console.log(stringTransformer("Example Input"));