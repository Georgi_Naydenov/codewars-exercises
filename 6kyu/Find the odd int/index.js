function findOdd(A) {
    const obj = {};

    A.forEach((number) => {
        obj[number] = (obj[number] || 0) + 1;
    });

    for (const element in obj) {
        if (obj[element] % 2 === 1) {
            return Number(element);
        }
    }
}

console.log(findOdd([1, 2, 2, 3, 3, 3, 4, 3, 3, 3, 2, 2, 1]));