function mixwords(input) {
    const words = input.split(' ');

    function shuffleWord(word) {
        if (word.length < 3) {
            return word;
        }
        const firstLetter = word[0];
        const lastLetter = word[word.length - 1];
        const interior = word.slice(1, -1).split('');
        for (let i = interior.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [interior[i], interior[j]] = [interior[j], interior[i]];
        }
        return firstLetter + interior.join('') + lastLetter;
    }
    const mixedWords = words.map(word => shuffleWord(word)).join(' ');

    return mixedWords;
}

console.log(mixwords('Winter is coming'));