function persistence(num) {
    let count = 0;

    while (num >= 10) {
        const numStr = num.toString();
        num = numStr.split('').reduce((acc, digit) => acc * parseInt(digit, 10), 1);
        count++;
    }
    return count;
}

console.log(persistence(4));