function numberOfPairs(gloves) {
    const glovePairs = {};
    let pairs = 0;

    for (let glove of gloves) {
        if (glovePairs[glove]) {
            glovePairs[glove] += 1;
        } else {
            glovePairs[glove] = 1;
        }
    }

    for (let pair in glovePairs) {
        pairs += Math.floor(glovePairs[pair] / 2);
    }
    return pairs;
}

console.log(numberOfPairs(["red", "red", "red", "red", "red", "red"]));