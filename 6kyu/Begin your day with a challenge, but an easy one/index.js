function oneTwoThree(n) {
    if (n === 0) return ['0', '0'];
    return ["9".repeat(n / 9) + (n % 9 ? n % 9 : ""), `1`.repeat(n)]
}

console.log(oneTwoThree(19));