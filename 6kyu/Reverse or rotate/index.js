function revrot(str, sz) {
    if (sz <= 0 || str === '' || sz > str.length) return '';

    function isSumOfCubesEven(chunk) {
        let sum = 0;
        for (let digit of chunk) {
            sum += Math.pow(parseInt(digit, 10), 3);
        }
        return sum % 2 === 0;
    }

    let result = "";
    for (let i = 0; i < str.length; i += sz) {
        const chunk = str.slice(i, i + sz);
        if (chunk.length === sz) {
            if (isSumOfCubesEven(chunk)) {
                result += chunk.split("").reverse().join("");
            } else {
                result += chunk.slice(1) + chunk[0];
            }
        }
    }

    return result;
}


//console.log(revrot("66443875", 4));