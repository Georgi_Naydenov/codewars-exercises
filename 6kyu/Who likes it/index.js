function likes(names) {
    if (names.length > 3) {
        return `${names.slice(0, 2).join(', ')} and ${names.length - 2} others like this`;
    } else if (names.length === 3) {
        const [person1, person2, person3] = names;
        return `${person1}, ${person2} and ${person3} like this`;
    } else if (names.length === 2) {
        const [person1, person2] = names;
        return `${person1} and ${person2} like this`;
    } else if (names.length === 1) {
        return `${names[0]} likes this`;
    } else {
        return `no one likes this`;
    }
}

//console.log(likes(["Alex", "Jacob", "Mark", "Max", "Georgi"]));