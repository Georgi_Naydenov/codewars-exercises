function digitalRoot(n) {
    while (n >= 10) {
        n = sumOfDigits(n);
    }
    return n;
}

function sumOfDigits(num) {
    return String(num)
        .split('')
        .map(Number)
        .reduce((acc, digit) => acc + digit, 0);
}

console.log(digitalRoot(942));