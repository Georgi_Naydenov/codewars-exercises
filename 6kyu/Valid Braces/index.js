function validBraces(str) {
    const stack = [];
    const openingBraces = "([{";
    const closingBraces = ")]}";

    for (let i = 0; i < str.length; i++) {
        const char = str[i];

        if (openingBraces.includes(char)) {
            stack.push(char);
        } else if (closingBraces.includes(char)) {
            if (stack.length === 0) {
                return false;
            }

            const top = stack.pop();

            if (
                (char === ")" && top !== "(") ||
                (char === "]" && top !== "[") ||
                (char === "}" && top !== "{")
            ) {
                return false;
            }
        }
    }

    return stack.length === 0;
}

console.log(validBraces('([{}])'));