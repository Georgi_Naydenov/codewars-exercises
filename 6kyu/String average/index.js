function averageString(str) {
    const numberWords = {
        'zero': 0,
        'one': 1,
        'two': 2,
        'three': 3,
        'four': 4,
        'five': 5,
        'six': 6,
        'seven': 7,
        'eight': 8,
        'nine': 9
    };

    const words = str.split(" ");
    let sum = 0;

    for (let word of words) {
        const numericValue = numberWords[word];
        if (numericValue === undefined || numericValue > 9) {
            return 'n/a';
        }

        sum += numericValue;
    }
    
    const average = Math.floor(sum / words.length);
    const averageWord = Object.keys(numberWords).find(key => numberWords[key] === average);

    return averageWord;
}

console.log(averageString("zero zero zero zero zero"));