function towerBuilder(floors) {
    const tower = [];

    for (let i = 1; i <= floors; i++) {
        const spaces = ' '.repeat(floors - i);
        const stars = '*'.repeat(2 * i - 1);
        tower.push(spaces + stars + spaces);
    }

    return tower;
}

console.log(towerBuilder(2));