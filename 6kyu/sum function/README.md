# Sum function

Write a sum function which will work properly when invoked using either syntax below.

```javascript
sum(2,3);  // Outputs 5
sum(2)(3); // Outputs 5
```

[Link for exercise](https://www.codewars.com/kata/57ab606e53ba3339da0000a2/train/javascript)