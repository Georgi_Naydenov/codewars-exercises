// Return the output array, and ignore all non-op characters
function parse(data) {
    let currentState = 0;
    const result = [];
    const arrayData = data.split("");
    arrayData.forEach(item => {
        if (item === 'i') {
            currentState++;
        } else if (item === 'd') {
            currentState--;
        } else if (item === 's') {
            currentState = currentState * currentState;
        } else if (item === 'o') {
            result.push(currentState);
        }
    })
    return result;
}

console.log(parse('iiisdoso'));