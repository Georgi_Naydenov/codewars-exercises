function validPhoneNumber(phoneNumber) {
    const filterNumbers = phoneNumber.split("").filter(number => number.match(/\d+(\.\d+)?/g)).join("");
    return `(${filterNumbers.slice(0, 3)}) ${filterNumbers.slice(3, 6)}-${filterNumbers.slice(6)}` === phoneNumber ?
        true : false;
}

console.log(validPhoneNumber("(123) 456-7890"));