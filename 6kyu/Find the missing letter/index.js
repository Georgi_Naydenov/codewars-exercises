function findMissingLetter(array) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    const firstLetter = alphabet.indexOf(array[0]);
    const lastLetter = alphabet.indexOf(array[array.length - 1]) + 1;
    const sliced = alphabet.slice(firstLetter, lastLetter);
    return sliced.filter(letter => !array.includes(letter)).join(' ');
}

//console.log(findMissingLetter(['O', 'Q', 'R', 'S']));