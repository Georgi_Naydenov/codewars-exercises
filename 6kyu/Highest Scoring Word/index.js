function wordIntoNumber(word) {
    const alphabetArray = Array.from('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    const totalSum = [...word.toUpperCase()].reduce((acc, char) => acc + (alphabetArray.indexOf(char) + 1), 0);
    return [word, totalSum];
}

function high(x) {
    const allResults = x.split(" ").map(wordIntoNumber);
    const highestWord = allResults.sort((a, b) => b[1] - a[1])[0][0];
    return highestWord;
}

console.log(high('what time are we climbing up the volcano'));