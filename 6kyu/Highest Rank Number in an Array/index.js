function highestRank(arr) {
    const ranks = {};

    arr.forEach(element => {
        if (!ranks.hasOwnProperty(element)) {
            ranks[element] = 0;
        } else {
            ranks[element] = ranks[element] + 1;
        }
    });

    let maxKey = null;
    let maxValue = Number.NEGATIVE_INFINITY;

    for (const key in ranks) {
        if (ranks.hasOwnProperty(key)) {
            const value = ranks[key];
            if (value > maxValue || (value === maxValue && parseInt(key) > parseInt(maxKey))) {
                maxKey = key;
                maxValue = value;
            }
        }
    }
    return parseInt(maxKey);
}

console.log(highestRank([12, 10, 8, 12, 7, 6, 4, 10, 12, 10]));