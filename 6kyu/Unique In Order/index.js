var uniqueInOrder = function (iterable) {
    return [...iterable].filter((item, index, array) => item !== array[index - 1]);
}

console.log(uniqueInOrder('AAAABBBCCDAABBB'));