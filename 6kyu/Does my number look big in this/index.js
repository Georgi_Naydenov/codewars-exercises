function narcissistic(value) {
    const numStr = value.toString();
    const numDigits = numStr.length;
    
    const sum = numStr.split('').reduce((acc, digit) => acc + Math.pow(parseInt(digit, 10), numDigits), 0);
    return sum === value;
}

console.log(narcissistic(122));