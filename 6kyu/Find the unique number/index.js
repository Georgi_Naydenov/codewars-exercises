function findUniq(arr) {
    const numberCount = new Map();

    for (let num of arr) {
        if (numberCount.has(num)) {
            numberCount.set(num, numberCount.get(num) + 1);
        } else {
            numberCount.set(num, 1);
        }
    }

    for (let [num, count] of numberCount) {
        if (count === 1) {
            return num;
        }
    }
}

//console.log(findUniq([1, 1, 1, 2, 1, 1]));