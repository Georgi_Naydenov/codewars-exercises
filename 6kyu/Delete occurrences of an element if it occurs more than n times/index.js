function deleteNth(arr, n) {
    const result = [];
    const count = {};

    for (const num of arr) {
        count[num] = (count[num] || 0) + 1;
        if (count[num] <= n) {
            result.push(num);
        }
    }
    return result;
}

//console.log(deleteNth([1, 1, 3, 3, 7, 2, 2, 2, 2], 3));