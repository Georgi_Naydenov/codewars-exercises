function f(n, m) {
    return gauss(m - 1) * (n / m | 0) + gauss(n % m)
}

function gauss(n) {
    return n * (n + 1) / 2
}

console.log(f(10, 5));