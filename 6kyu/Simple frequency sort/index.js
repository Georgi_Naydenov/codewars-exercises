function solve(arr) {
    const frequencyMap = new Map();
    arr.forEach((num) => {
        frequencyMap.set(num, (frequencyMap.get(num) || 0) + 1);
    });
    arr.sort((a, b) => {
        const frequencyA = frequencyMap.get(a);
        const frequencyB = frequencyMap.get(b);
        if (frequencyA !== frequencyB) {
            return frequencyB - frequencyA;
        }
        return a - b;
    });
    
    return arr;
}

console.log(solve([2, 3, 5, 3, 7, 9, 5, 3, 7]));