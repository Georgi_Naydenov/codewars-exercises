function getLengthOfMissingArray(arrayOfArrays) {
    if (arrayOfArrays === null || arrayOfArrays.length === 0 || arrayOfArrays.some(array => array === null || array.length === 0)) {
        return 0;
    }
    let missingLength;
    const arrayLengths = arrayOfArrays.map(array => array.length).sort((a, b) => a - b);

    for (let i = 0; i < arrayLengths.length; i++) {
        if (arrayLengths[i] === 0) {
            return 0;
        }
        if (arrayLengths[i] + 1 !== arrayLengths[i + 1] && arrayLengths[i + 1] !== undefined) {
            return arrayLengths[i] + 1;
        }
    }
    return missingLength;
}


console.log(getLengthOfMissingArray([
    [1, 2],
    [4, 5, 1, 1],
    [1],
    [5, 6, 7, 8, 9]
]));