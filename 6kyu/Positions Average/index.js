function posAverage(s) {
    const numbers = s.split(', ');
    let totalMatches = 0;
    let totalPositions = 0;

    for (let position = 0; position < numbers[0].length; position++) {
        for (let i = 0; i < numbers.length; i++) {
            for (let j = i + 1; j < numbers.length; j++) {
                if (numbers[i][position] === numbers[j][position]) {
                    totalMatches++;
                }
                totalPositions++;
            }
        }
    }

    const average = (totalMatches / totalPositions) * 100;

    return average.toFixed(10);
}


console.log(posAverage('466960, 069060, 494940, 060069, 060090, 640009, 496464, 606900, 004000, 944096'));