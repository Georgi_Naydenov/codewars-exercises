function wave(str) {
    const waveResult = [];
    
    for (let i = 0; i < str.length; i++) {
        if (/[a-z]/.test(str[i])) {
            const wavedStr = str.slice(0, i) + str[i].toUpperCase() + str.slice(i + 1);
            waveResult.push(wavedStr);
        }
    }
    return waveResult;
}

console.log(wave("two words"));