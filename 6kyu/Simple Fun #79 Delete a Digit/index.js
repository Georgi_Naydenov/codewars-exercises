function deleteDigit(n) {
    const numbers = n.toString().split("").map(Number);
  
    for (let i = 0; i < numbers.length - 1; i++) {
      if (numbers[i] < numbers[i + 1]) {
        numbers.splice(i, 1);
        return Number(numbers.join(""));
      }
    }
    numbers.pop();
    return Number(numbers.join(""));
  }

console.log(deleteDigit(152));