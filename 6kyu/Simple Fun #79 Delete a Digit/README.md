# Simple Fun #79: Delete a Digit

Given an integer n, find the maximal number you can obtain by deleting exactly one digit of the given number.

## Example

For n = 152, the output should be 52;

For n = 1001, the output should be 101.