# Array Deep Count

You are given an array. Complete the function that returns the number of ALL elements within an array, including any nested arrays.

## Examples

```javascript
[]                   -->  0
[1, 2, 3]            -->  3
["x", "y", ["z"]]    -->  4
[1, 2, [3, 4, [5]]]  -->  7
```
[Link for exercise](https://www.codewars.com/kata/596f72bbe7cd7296d1000029/train/javascript)