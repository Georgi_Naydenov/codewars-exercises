function deepCount(a) {
    const totalProducts = a.reduce((acc, val) => {
        return acc + (Array.isArray(val) ? deepCount(val) : 0);
    }, a.length);
    return totalProducts;
}

console.log(deepCount([1, 2, [3, 4, [5]]]));