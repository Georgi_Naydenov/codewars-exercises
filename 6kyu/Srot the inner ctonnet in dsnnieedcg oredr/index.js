function sortWord(word) {
    const firstLetter = word.charAt(0);
    const lastLetter = word.charAt(word.length - 1);
    const modifiedWord = word.substring(1, word.length - 1);
    return firstLetter + modifiedWord.split('').sort((a, b) => b.localeCompare(a)).join('') + lastLetter;
}

function sortTheInnerContent(words) {
    const sortedSentence = words
        .split(" ")
        .map(word => (word.length > 3 ? sortWord(word) : word))
        .join(" ");

    return sortedSentence;
}

console.log(sortTheInnerContent("sort the inner content in descending order"));