function alphabetPosition(text) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    const result = [];

    for (let char of text.toLowerCase()) {
        if (alphabet.includes(char)) {
            const index = alphabet.indexOf(char);
            result.push(index + 1);
        }
    }
    return result.join(' ');
}

//console.log(alphabetPosition("The sunset sets at twelve o' clock."));