function getParticipants(handshakes) {
    const discriminant = 1 + 8 * handshakes;
    const positiveRoot = Math.ceil((1 + Math.pow(discriminant, 0.5)) / 2);
    return (positiveRoot > 1) ? positiveRoot : 0;
}

console.log(getParticipants(7));