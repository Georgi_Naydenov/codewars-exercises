function telephoneWordsHelper(digitString, currentIndex, currentWord, result, digitToLetters) {
    if (currentIndex === digitString.length) {
        result.push(currentWord);
        return;
    }

    const currentDigit = digitString[currentIndex];
    const letters = digitToLetters[currentDigit];

    for (const letter of letters) {
        telephoneWordsHelper(digitString, currentIndex + 1, currentWord + letter, result, digitToLetters);
    }
}

function telephoneWords(digitString) {
    if (!digitString || digitString.length !== 4) {
        return [];
    }

    const digitToLetters = {
        '0': ['0'],
        '1': ['1'],
        '2': ['A', 'B', 'C'],
        '3': ['D', 'E', 'F'],
        '4': ['G', 'H', 'I'],
        '5': ['J', 'K', 'L'],
        '6': ['M', 'N', 'O'],
        '7': ['P', 'Q', 'R', 'S'],
        '8': ['T', 'U', 'V'],
        '9': ['W', 'X', 'Y', 'Z'],
    };

    const result = [];
    telephoneWordsHelper(digitString, 0, '', result, digitToLetters);

    return result;
}


console.log(telephoneWords('2357'));