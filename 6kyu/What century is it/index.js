function whatCentury(year) {
    const century = Math.ceil(year / 100);
    const lastDigit = century % 10;
    
    if (lastDigit === 1 && century !== 11) {
        return century + "st";
    } else if (lastDigit === 2 && century !== 12) {
        return century + "nd";
    } else if (lastDigit === 3 && century !== 13) {
        return century + "rd";
    } else {
        return century + "th";
    }
}


console.log(whatCentury('1999'));