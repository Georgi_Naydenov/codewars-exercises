function getOrder(input) {
    const menu = 'Burger Fries Chicken Pizza Sandwich Onionrings Milkshake Coke'.split(' ');
    const order = [];

    for (const food of menu) {
        while (input.includes(food.toLowerCase())) {
            order.push(food);
            input = input.replace(food.toLowerCase(), '');
        }

    }
    return order.join(" ");
}

console.log(getOrder('milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza'));