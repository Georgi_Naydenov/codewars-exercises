function longestPalindrome(s) {
    if (!s || s.length === 0) {
        return 0;
    }

    const string = s.split('');
    let maxLength = 1;

    for (let i = 0; i < string.length; i++) {
        for (let j = i + 1; j < string.length; j++) {
            const substring = string.slice(i, j + 1).join('');
            if (palindrome(substring) && substring.length > maxLength) {
                maxLength = substring.length;
            }
        }
    }

    return maxLength;
}

function palindrome(str) {
    return str === str.split("").reverse().join('');
}

console.log(longestPalindrome('zzbaabcd'));
