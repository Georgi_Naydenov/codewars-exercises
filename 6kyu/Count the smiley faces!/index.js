function countSmileys(arr) {
    let countSmileEmoji = 0;
    if (arr.length === 0) return 0;
    arr.forEach(item => {
        if (/[:;][-~]?[)D]/.test(item)) {
            countSmileEmoji++;
        }
    });
    return countSmileEmoji;
}

console.log(countSmileys([":---)", "))", ";~~D", ";D"])); // Output: 1