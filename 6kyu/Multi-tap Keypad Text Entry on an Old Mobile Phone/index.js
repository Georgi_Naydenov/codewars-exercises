function presses(phrase) {
    const keypad = [
        "1", "ABC2", "DEF3",
        "GHI4", "JKL5", "MNO6",
        "PQRS7", "TUV8", "WXYZ9",
        "*", " 0", "#"
      ];
      let count = 0;
      for (let i = 0; i < phrase.length; i++) {
        const char = phrase[i].toUpperCase();
        for (let j = 0; j < keypad.length; j++) {
          const index = keypad[j].indexOf(char);
          if (index !== -1) {
            count += index + 1;
            break;
          }
        }
      }
      return count;
}

console.log(presses('HOW R U'));