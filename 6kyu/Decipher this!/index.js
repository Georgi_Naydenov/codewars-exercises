function decipherThis(str) {
    const sentenceArray = str.split(" ");
    const decipheredWords = [];

    for (let i = 0; i < sentenceArray.length; i++) {
        const word = sentenceArray[i];
        const digits = word.match(/^\d+/);

        if (digits) {
            const asciiCode = parseInt(digits[0]);
            const firstLetter = String.fromCharCode(asciiCode);
            const restOfWord = word.slice(digits[0].length);

            const modifiedWord = restOfWord.length > 1 ?
                firstLetter + restOfWord[restOfWord.length - 1] + restOfWord.slice(1, -1) + restOfWord[0] :
                firstLetter + restOfWord;

            decipheredWords.push(modifiedWord);
        }
    }

    return decipheredWords.join(' ');
}

// Example
console.log(decipherThis('72olle 103doo 100ya')); // 'Hello good day'