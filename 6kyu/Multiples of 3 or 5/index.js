function solution(number) {
    if (number < 0) return 0;
    const allNumbers = [];

    for (let i = 1; i < number; i++) {
        if (i % 3 === 0 || i % 5 === 0) {
            allNumbers.push(i);
        }
    }
    return allNumbers.reduce((acc, curr) => acc + curr, 0);
}

console.log(solution(10));