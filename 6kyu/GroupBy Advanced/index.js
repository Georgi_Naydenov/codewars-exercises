function groupBy(array, classifier, downstream, accumulatorSupplier) {
    const map = new Map();

    for (const obj of array) {
        const key = classifier(obj);

        if (!map.has(key)) {
            map.set(key, accumulatorSupplier());
        }

        map.set(key, downstream(map.get(key), obj));
    }

    return map;
}


const employees = [{
        name: "James",
        income: 1000,
        profession: "developer",
        age: 23
    },
    {
        name: "Robert",
        income: 1100,
        profession: "qa",
        age: 34
    },
    {
        name: "John",
        income: 1200,
        profession: "designer",
        age: 32
    },
    {
        name: "Mary",
        income: 1300,
        profession: "designer",
        age: 22
    },
    {
        name: "Patricia",
        income: 1400,
        profession: "qa",
        age: 23
    },
    {
        name: "Jennifer",
        income: 1500,
        profession: "developer",
        age: 45
    },
    {
        name: "Max",
        income: 1600,
        profession: "developer",
        age: 27
    },
];

const profession2totalIncome = groupBy(
    employees,
    (employee) => employee.profession,
    (acc, employee) => acc + employee.income,
    () => 0
);

console.log(profession2totalIncome);