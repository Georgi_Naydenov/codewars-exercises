# String array duplicates

In this Kata, you will be given an array of strings and your task is to remove all consecutive duplicate letters from each string in the array.

For example:

```javascript
    dup(["abracadabra","allottee","assessee"]) = ["abracadabra","alote","asese"].

    dup(["kelless","keenness"]) = ["keles","kenes"].
```

Strings will be lowercase only, no spaces. See test cases for more examples.

[Link for exercise](https://www.codewars.com/kata/59f08f89a5e129c543000069/train/javascript)