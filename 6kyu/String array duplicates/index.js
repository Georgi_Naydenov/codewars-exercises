function dup(s) {
    return s.map((word) => removeConsecutiveDuplicates(word));
};

function removeConsecutiveDuplicates(inputString) {
    let result = inputString[0];

    for (let i = 1; i < inputString.length; i++) {
        if (inputString[i] !== inputString[i - 1]) {
            result += inputString[i];
        }
    }
    return result;
}

console.log(dup(["ccooddddddewwwaaaaarrrrsssss", "piccaninny", "hubbubbubboo"]));