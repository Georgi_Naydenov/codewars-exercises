function order(words) {
    return words.split(' ').sort((a, b) => {
        const numA = Number(a.match(/\d+/)[0]);
        const numB = Number(b.match(/\d+/)[0]);
        return numA - numB;
    }).join(' ');
}

console.log(order('is2 Thi1s T4est 3a'));