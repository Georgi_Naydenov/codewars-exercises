// Implement a stack data structure (push, pop, peek operations).

class Stack {
    constructor() {
        this.array = [];
    }

    push(item) {
        this.array.push(item);
    }

    pop() {
        if (this.isEmpty()) {
            throw new Error('Stack is empty!');
        }
        this.array.pop(item);
    }

    peek() {
        if (this.isEmpty()) {
            throw new Error('Stack is empty!');
        }
        return this.array[this.array.length - 1];
    }

    isEmpty() {
        if (this.array.length === 0) {
            return true;
        } else {
            return false;
        }
    }

    clear() {
        this.array = [];
    }

}

const stack = new Stack();

stack.push('one');
stack.push('two');

console.log(stack.peek());
