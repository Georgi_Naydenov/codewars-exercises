// Write a function to print Fibonacci numbers up to N.

function fibonacci(num) {
    const numbers = [0, 1];
    for (let i = 2; i <= num; i++) {
        numbers[i] = numbers[i - 1] + numbers[i - 2];
    }
    return numbers.join(' ');
}

console.log(fibonacci(5));