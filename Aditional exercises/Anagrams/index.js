// Write a function to check if two Strings are anagrams in Java?

function areAnagrams(str1, str2) {
    const cleanedStr1 = str1.replace(/\s/g, '').toLowerCase();
    const cleanedStr2 = str2.replace(/\s/g, '').toLowerCase();

    if (cleanedStr1.length !== cleanedStr2.length) {
        return false;
    }
    const sortedStr1 = cleanedStr1.split('').sort().join('');
    const sortedStr2 = cleanedStr2.split('').sort().join('');

    return sortedStr1 === sortedStr2;
}

const str1 = "listen";
const str2 = "silent";

console.log(areAnagrams(str1, str2));