const employee1 = {
    name: "Georgi",
    age: 42,
    friends: ['Monika', 'Qnitsa', 'Boyan'],
    address: {
        street: 'one one',
        country: {
            name: "bulgaria",
            continent: "Europe"
        }
    }
};


function deepCopyFunc(object) {
    const result = {};

    if (typeof object !== 'object' || typeof object === undefined || typeof object === null ||
        Array.isArray(object) || typeof object === 'function') {
        return object;
    }

    const keys = Object.keys(object);

    for (const key in keys) {
        result[keys[key]] = deepCopyFunc(object[keys[key]])
    }
    return result;
};


console.log(deepCopyFunc(employee1));