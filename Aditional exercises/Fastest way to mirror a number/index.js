//  What is the fastest way of mirroring a number? 12 -> 21


function mirror(number) {
    return parseInt(number.toString().split("").reverse().join(""));
}

console.log(mirror(21));