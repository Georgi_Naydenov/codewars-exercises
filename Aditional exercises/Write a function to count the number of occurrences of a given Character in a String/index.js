//Write a function to count the number of occurrences of a given Character in a String?


const countOccurances = (string, charToCheckForOccurances) => {
    return string.split("").filter(letter => letter === charToCheckForOccurances).length;
}

console.log(countOccurances('this is a test', 'i'));