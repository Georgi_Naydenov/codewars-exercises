// Write a function that takes a String and reverses it?

function reverseString(str) {
    return str.split("").reverse().join('')
}

console.log(reverseString("Hello, World!")); 