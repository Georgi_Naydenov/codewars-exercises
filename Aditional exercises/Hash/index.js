class HashTable {
    constructor(size = 10) {
        this.size = size;
        this.table = new Array(size).fill(null).map(() => []);
    }

    hash(key) {
        let hashValue = 0;
        for (let i = 0; i < key.length; i++) {
            hashValue += key.charCodeAt(i);
        }
        return hashValue % this.size;
    }

    insert(key, value) {
        const index = this.hash(key);
        this.table[index].push({
            key,
            value
        });
    }

    search(key) {
        const index = this.hash(key);
        for (const entry of this.table[index]) {
            if (entry.key === key) {
                return entry.value;
            }
        }
        return null;
    }

    delete(key) {
        const index = this.hash(key);
        const bucket = this.table[index];
        for (let i = 0; i < bucket.length; i++) {
            if (bucket[i].key === key) {
                bucket.splice(i, 1);
                return true;
            }
        }
        return false;
    }
}

const myHashTable = new HashTable();

myHashTable.insert("name", "John");
myHashTable.insert("age", 25);
myHashTable.insert("city", "New York");

console.log("Name:", myHashTable.search("name"));

myHashTable.delete("age");
console.log("Age:", myHashTable.search("age"));