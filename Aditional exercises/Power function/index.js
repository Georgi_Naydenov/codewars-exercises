function power(b, e) {
    if (e === 0) {
        return 1;
    }
    return b * power(b, e - 1);
}

// Example usage:
console.log(power(2, 3)); // Should output 8
console.log(power(5, 2)); // Should output 25

function isPalindrome(str) {
    if (str.length <= 1) {
        return true;
    }
    if (str.charAt(0) === str.charAt(str.length - 1)) {
        return isPalindrome(str.substring(1, str.length - 1));
    } else {
        return false;
    }
}

// Example usage:
console.log(isPalindrome("radar")); // Should output true
console.log(isPalindrome("hello")); // Should output false
console.log(isPalindrome("level")); // Should output true