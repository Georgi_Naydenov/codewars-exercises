# Problem: Power Function

Write a recursive function named power that takes two arguments, a base b and an exponent e, and calculates the result of raising the base to the power of the exponent.

## For example:
```javascript
    power(2, 3) should return 8 (2^3 = 2 * 2 * 2 = 8).
    power(5, 2) should return 25 (5^2 = 5 * 5 = 25).
```

Your task is to implement the power function using recursion.