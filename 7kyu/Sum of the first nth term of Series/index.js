function SeriesSum(n) {
    if(n === 0) return '0.00';
    let result = 0;

    for (let i = 0; i < n; i++) {
        result += 1 / (1 + i * 3);
        
    }
    return result.toFixed(2);
}

console.log(SeriesSum(3));