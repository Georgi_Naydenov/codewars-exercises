function seaSick(x) {
    const seaLength = x.length;
    let levelChanges = 0;

    for (let i = 1; i < seaLength; i++) {
        if (x[i] !== x[i - 1]) {
            levelChanges++;
        }
    }
    return (levelChanges / seaLength) * 100 > 20 ? "Throw Up" : "No Problem";
}

console.log(seaSick("~"));