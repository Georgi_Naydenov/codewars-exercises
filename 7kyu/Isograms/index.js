function isIsogram(str) {
    const setIsogram = new Set(str.toLowerCase());
    const stringIsogram = Array.from(setIsogram).join('');
    return stringIsogram === str.toLowerCase() ? true : false;
}

console.log(isIsogram('Dermatoglyphics'));