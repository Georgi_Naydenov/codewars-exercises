// function findDeletedNumber(arr, mixArr) {
//     const sortedArr = arr.sort();
//     const sortedMixArr = mixArr.sort();

//     for (let i = 0; i < sortedArr.length; i++) {
//         if (sortedArr[i] !== sortedMixArr[i]) {
//             return sortedArr[i];
//         }
//     }
//     return 0;
// }


function findDeletedNumber(arr, mixArr) {
    const sortedArr = arr.sort((a, b) => a - b);
    const sortedMixArr = mixArr.sort((a, b) => a - b);

    return sortedArr.find((num, index) => num !== sortedMixArr[index]) || 0;
}



console.log(findDeletedNumber([1, 2, 3, 4, 5], [3, 4, 1, 5]));