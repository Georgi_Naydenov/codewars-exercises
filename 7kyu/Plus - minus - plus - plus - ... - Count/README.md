# Plus - minus - plus - plus - ... - Count

Count how often sign changes in array.

## Result

number from 0 to ... . Empty array returns 0

```javascript
const arr = [1, -3, -4, 0, 5];
=> 2 
```


[Link for exercise](https://www.codewars.com/kata/5bbb8887484fcd36fb0020ca/train/javascript)