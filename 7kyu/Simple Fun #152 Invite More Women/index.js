function inviteMoreWomen(L) {
    const women = L.filter(woman => woman === -1).length;
    const men = L.filter(man => man === 1).length;
    return women >= men ? false : true;
}

console.log(inviteMoreWomen([1, -1]));