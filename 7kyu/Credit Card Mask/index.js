function maskify(cc) {
    const maskedSymbols = cc.length - 4;
    return cc.length <= 4 ? cc : `#`.repeat(maskedSymbols) + cc.slice(cc.length - 4);
}

console.log(maskify('4556364607935616'));