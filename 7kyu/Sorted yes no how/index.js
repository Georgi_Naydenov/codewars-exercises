function isSortedAndHow(array) {

    if (array.every((num, index, array) => index === 0 || num >= array[index - 1])) {
        return 'yes, ascending';
    }

    if (array.every((num, index, array) => index === 0 || num <= array[index - 1])) {
        return 'yes, descending';
    }
    return 'no';
}


console.log(isSortedAndHow([1, 2, 3, 4, 7]));