function XO(str) {
    const result = str.split('').reduce((count, char) => {
        const lowerChar = char.toLowerCase();
        count[lowerChar] = (count[lowerChar] || 0) + 1;
        return count;
    }, {});

    return result.x === result.o;
}

console.log(XO('xxOo'));