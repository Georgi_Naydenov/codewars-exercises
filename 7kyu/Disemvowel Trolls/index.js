function disemvowel(str) {
    const vowelRegex = /[aeiou]/gi;
    return str.split('').filter(letter => (!letter.match(vowelRegex))).join('');
}

//console.log(disemvowel('This website is for losers LOL!'));