function minMax(arr) {
    const minValue = Math.min(...arr);
    const maxValue = Math.max(...arr);
    return [minValue, maxValue];
}


console.log(minMax([1, 24, 553]));