function switcheroo(x) {
    return x.split("").map(letter => {
        if (letter === 'a') {
            return letter = 'b';
        } else if (letter === 'b') {
            return letter = 'a';
        } else {
            return letter;
        }
    }).join("");
}

console.log(switcheroo(`aaabcccbaaa`));