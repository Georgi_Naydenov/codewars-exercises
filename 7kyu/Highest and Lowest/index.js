function highAndLow(numbers) {
    const numberArray = numbers.split(' ').map(Number);
    const lowestNumber = Math.min(...numberArray);
    const highestNumber = Math.max(...numberArray);
    return `${highestNumber} ${lowestNumber}`;
}

console.log(highAndLow("1 2 3 4 5"));