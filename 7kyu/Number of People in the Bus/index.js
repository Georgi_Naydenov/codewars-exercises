var number = function (busStops) {
    let peopleInBus = 0;
    
    for (let i = 0; i < busStops.length; i++) {
        const currentPeople = busStops[i];
        peopleInBus += currentPeople[0];
        peopleInBus -= currentPeople[1];
    }
    return peopleInBus;
}

const input = number([
    [10, 0],
    [3, 5],
    [5, 8]
]);
console.log(input);