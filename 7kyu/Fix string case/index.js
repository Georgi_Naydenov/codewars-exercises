function solve(s) {
    const lowerCaseChars = s.split('').filter(letter => letter === letter.toLowerCase()).length;
    const upperCaseChars = s.split('').filter(letter => letter === letter.toUpperCase()).length;
   
    return lowerCaseChars >= upperCaseChars ? s.toLowerCase() : s.toUpperCase();
}

console.log(solve("CODe"));