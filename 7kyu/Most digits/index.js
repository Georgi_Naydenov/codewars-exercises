function findLongest(array) {
    let mostDigitNum = 0;

    for (let i = 0; i < array.length; i++) {
        if (String(array[i]).length > String(mostDigitNum).length) {
            mostDigitNum = array[i];
        }
    }
    return mostDigitNum;
}

console.log(findLongest([1, 10, 100]));