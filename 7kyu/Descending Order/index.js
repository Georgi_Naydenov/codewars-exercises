function descendingOrder(n) {
    const digits = n.toString().split('');
    let swapped;
    do {
        swapped = false;
        for (let i = 0; i < digits.length - 1; i++) {
            if (digits[i] < digits[i + 1]) {
                const temp = digits[i];
                digits[i] = digits[i + 1];
                digits[i + 1] = temp;
                swapped = true;
            }
        }
    } while (swapped);

    return parseInt(digits.join(''), 10);
}

console.log(descendingOrder(1023));