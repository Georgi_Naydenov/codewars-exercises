function cubeOdd(arr) {
    const numberRegex = /^-?\d+$/;
    const filteredStrings = arr.filter(element => !numberRegex.test(element));
    if (filteredStrings.length > 0) {
        return undefined;
    }
    const filteredOddNumbers = arr.filter(num => Math.abs(num) % 2 === 1);
    return filteredOddNumbers.map(num => Math.pow(num, 3)).reduce((acc, curr) => acc + curr, 0);
}

console.log(cubeOdd([-3, -2, 2, 3]));