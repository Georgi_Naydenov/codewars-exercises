# Sum of Odd Cubed Numbers

Find the sum of the odd numbers within an array, after cubing the initial integers. The function should return undefined if any of the values aren't numbers.

[Link for exercise](https://www.codewars.com/kata/580dda86c40fa6c45f00028a/train/javascript)