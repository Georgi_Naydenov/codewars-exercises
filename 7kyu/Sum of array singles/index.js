function repeats(arr) {
    const singleNumbers = arr.filter(number => arr.indexOf(number) === arr.lastIndexOf(number));
    return singleNumbers.reduce((acc, curr) => acc + curr, 0);
};

console.log(repeats([4, 5, 7, 5, 4, 8])); //15