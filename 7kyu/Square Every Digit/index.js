function squareDigits(num) {
    const arrayOfNums = num.toString().split('').map(Number);
    return +arrayOfNums.map(number => number * number).join('');
}

console.log(squareDigits(3212));