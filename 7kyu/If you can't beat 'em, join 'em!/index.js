const sumArray = array => array.reduce((acc, curr) => acc + curr, 0);

function cantBeatSoJoin(numbers) {
    
    const sortedNumbers = numbers.sort((a, b) => sumArray(b) - sumArray(a));
    return sortedNumbers.flat(1);
}


console.log(cantBeatSoJoin([
    [1, 2],
    [3, 4],
    [5, 6],
    [7, 8],
    [9]
]));