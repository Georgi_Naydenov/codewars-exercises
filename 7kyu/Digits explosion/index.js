function explode(s) {
    const splittedNumber = s.toString().split('');
    return splittedNumber.map(number => number.repeat(+number)).join("");
}

console.log(explode(321));