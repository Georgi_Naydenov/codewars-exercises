function accum(s) {
    let result = '';
    let count = 1;

    for (let i = 0; i < s.length; i++) {
        if (i > 0) {
            result += '-';
        }
        result += s[i].toUpperCase() + s[i].toLowerCase().repeat(count - 1);
        count++;
    }

    return result;
}

//console.log(accum('abcd'));