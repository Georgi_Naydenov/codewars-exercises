function solution(pairs) {
    let result = '';

    for (let key in pairs) {
        result += `${key} = ${pairs[key]},`

    }
    return result.slice(0, -1);
}

console.log(solution({
    a: 1,
    b: '2'
}));