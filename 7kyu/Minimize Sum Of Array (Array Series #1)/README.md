# Minimize Sum Of Array (Array Series #1) 

## Task

Given an array of integers , Find the minimum sum which is obtained from summing each Two integers product .

Array/list will contain positives only

Array/list will always have even size

## Explanation:

The minimum sum obtained from summing each two integers product ,  5*2 + 3*4 = 22

The minimum sum obtained from summing each two integers product ,  26*3 + 24*6 + 12*10 = 342

The minimum sum obtained from summing each two integers product ,  9*0 + 8*2 +7*4 +6*5 = 74

[Link for exercise](https://www.codewars.com/kata/5a523566b3bfa84c2e00010b/train/javascript)