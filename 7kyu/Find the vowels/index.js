function vowelIndices(word) {
    const vowelRegex = /[aeiouy]/i;
    return word.split("").map((letter, index) => {
        if (vowelRegex.test(letter)) {
            return index + 1
        }
        return -1;
    }).filter(number => number !== -1);
}

console.log(vowelIndices('apple'));